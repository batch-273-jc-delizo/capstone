class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }

  checkOut() {
    if (!this.cart.isEmpty()) {
      const total = this.cart.computeTotal();
      const order = {
        products: this.cart.showCartContents(),
        totalAmount: total.totalAmount
      };
      this.orders.push(order);
      this.cart.clearCartContents();
    }
    return this;
  }

}

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(product, quantity) {
    this.contents.push({ product, quantity });
    return this.contents;
  }

  showCartContents() {
    return this.contents;
  }

  updateProductQuantity(product, newQuantity) {
    for (let i = 0; i < this.contents.length; i++) {
      if (this.contents[i].product === product) {
        this.contents[i].quantity = newQuantity;
        break;
      }
    }
    return this.contents;
  }

  clearCartContents() {
    this.contents = [];
    this.totalAmount = 0;
    return this.contents;
  }

  computeTotal() {
    let total = 0;
    for (let i = 0; i < this.contents.length; i++) {
      total += this.contents[i].product.price * this.contents[i].quantity;
    }
    this.totalAmount = total;
    return this;
  }
  
  isEmpty() {
    return this.contents.length === 0;
  }

  getContents() {
    return this.contents;
  }

  getTotal() {
    return this.totalAmount;
  }
}



class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }

  archive() {
    if (this.isActive) {
      this.isActive = false;
    }
  }

  updatePrice(newPrice) {
    this.price = newPrice;
  }
}


const Jane = new Customer('jane@example.com');
const John = new Customer('john@example.com');
const Mary = new Customer('mary@example.com');
const Bob = new Customer('bob@example.com');
const Alice = new Customer('alice@example.com');
const Dave = new Customer('dave@example.com');
const Susan = new Customer('susan@example.com');

const Widget = new Product('Widget', 19.99);
const Gadget = new Product('Gadget', 9.99);
const Thingamabob = new Product('Thingamabob', 29.99);
const Doohickey = new Product('Doohickey', 14.99);
const Gizmo = new Product('Gizmo', 39.99);
const Contraption = new Product('Contraption', 49.99);
const Gimmick = new Product('Gimmick', 7.99);

Jane.cart.addToCart(Widget, 2);
Jane.cart.addToCart(Gadget, 1);
// Jane.cart.computeTotal();

// John.cart.addToCart(Thingamabob, 3);
// John.cart.computeTotal();

Mary.cart.addToCart(Doohickey, 4);
// Mary.cart.computeTotal();

Bob.cart.addToCart(Gizmo, 2);
Bob.cart.addToCart(Contraption, 1);
// Bob.cart.computeTotal();

Alice.cart.addToCart(Gimmick, 5);
// Alice.cart.computeTotal();

// Dave.cart.addToCart(Widget, 1);
// Dave.cart.addToCart(Gadget, 2);
// Dave.cart.addToCart(Thingamabob, 1);
// Dave.cart.computeTotal();

// Susan.cart.addToCart(Doohickey, 3);
// Susan.cart.addToCart(Gizmo, 1);
// Susan.cart.computeTotal();


